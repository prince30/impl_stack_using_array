package impl.stack.using.array;

public class Stack {
	private String[] items;
	private int capacity, top, size;

	public Stack(int capacity) throws Exception {
		if (capacity < 1) {
			throw new Exception("size of the stack less then 1.");

		}
		this.capacity = capacity;
		items = new String[capacity];
		size = 0;
		top = -1;
	}

	public void push(String item) {
		if (isFull()) {
			throw new ArrayIndexOutOfBoundsException("stack overflow");
		}

		size++;
		top++;
		items[top++] = item;
	}

	public String pop() throws StackException {
		if (isFull()) {
			throw new StackException();
		}

		top--;
		size--;
		return items[top--];
	}

	public int size() {
		return size;
	}

	public String peek() {
		return items[top];
	}

	public boolean isFull() {
		if (this.size == this.capacity) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		} else {
			return false;
		}
	}
}
