package impl.stack.using.array;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class StackTest {

	@Test
	public void testPushAndPop() throws Exception {
		Stack stack = new Stack(100);
		stack.push("welcome");
		stack.push("hello");
		stack.push("world");
		stack.push("newzan");
		stack.push("comapny");
		stack.pop();
		stack.pop();
		assertEquals("world", stack.pop());
	}

	@Test
	public void testCheckEmpty() throws Exception {
		Stack stack = new Stack(100);
		stack.push("welcome");
		stack.push("hello");
		stack.push("world");
		stack.push("newzan");
		stack.push("comapny");
		stack.pop();
		stack.pop();
		stack.pop();
		stack.pop();
		stack.pop();
		assertEquals(true, stack.isEmpty());
	}

	@Test
	public void testCheckFuulOrEmpty() throws Exception {
		Stack stack = new Stack(100);
		stack.push("welcome");
		stack.push("hello");
		assertEquals(false, stack.isEmpty());
		assertEquals(false, stack.isEmpty());
		assertEquals(false, stack.isEmpty());
		assertEquals(false, stack.isFull());
	}

	@Test
	public void testCheckFullOrEmpty() throws Exception {
		Stack stack = new Stack(100);
		stack.push("welcome");
		stack.push("hello");
		assertEquals(false, stack.isFull());
		assertEquals(false, stack.isFull());
		assertEquals(false, stack.isFull());
		assertEquals(false, stack.isEmpty());
		assertEquals(2, stack.size());
	}

	/*
	 * Expected output: call isFull return true, stack is full
	 */
	@Test
	public void testCheckFull() throws Exception {
		Stack stack = new Stack(100);
		stack.push("welcome");
		stack.push("hello");
		stack.push("world");
		stack.push("newzan");
		stack.push("comapny");
		assertEquals(false, stack.isFull());
		assertEquals(false, stack.isFull());
		assertEquals(false, stack.isFull());
	}

}
